import os
import pandas as pd

configfile: 'config.yaml'

samples = pd.read_table("samples.csv", header=0, sep=',', index_col=0)
lengths = list(range(config['min_length'],config['max_length']+1))

hairpin = os.path.join(config['reference_folder'],config['hairpin_ref_file'])
mature = os.path.join(config['reference_folder'],config['mature_db_file'])

rule all:
	input: 	
		'isomir_readcount.txt',
		expand('data/{sample}_isomir_unmapped.fastq', sample=samples.index)


rule isomir_fasta_generation:
	input:
		hairpin='{}.fa'.format(hairpin),
		mature_db='{mature}.fa'
	output:
		'{mature}_isomir.fa'
	shell:
		"""python src/create_isomir_ref.py -H {input.hairpin} -M {input.mature_db} -3T refs/3prime_template.json -5T refs/5prime_template.json  -OUT {output} -MOUT multireads.txt"""

rule isomir_fasta_splitting:
	input: temp('{mature}_isomir.fa')
	output: expand('{{mature}}_isomir_{length}nt.txt', length=lengths)
	shell:
		"""awk -f  src/fasta_file_splitting.awk {input}"""


def get_prefix(wildcards):
	return('{}_isomir_{}nt'.format(mature,wildcards.length))

rule isomir_alignment_index:
	input:
		file = '{mature}_isomir_{length}nt.txt',
	output:
		reference1='{mature}_isomir_{length}nt.1.ebwt',
		reference2='{mature}_isomir_{length}nt.2.ebwt',
		reference3='{mature}_isomir_{length}nt.3.ebwt',
		reference4='{mature}_isomir_{length}nt.4.ebwt',
		referencerev1='{mature}_isomir_{length}nt.rev.1.ebwt',
		referencerev2='{mature}_isomir_{length}nt.rev.2.ebwt'
	params:
		prefix = lambda wildcards: get_prefix(wildcards)
	shell:
		"""bowtie-build {input} {params.prefix}"""

rule fastq_splitting:
	input: temp('data/{sample}.fastq')
	output: expand('data/{{sample}}_{length}nt.txt', length=lengths)
	shell:
		"""awk -f  src/fastq_file_splitting.awk {input}"""

rule bowtie_alignment:
	input: 
		file='data/{sample}_{length}nt.txt',
		reference1='{}_isomir_{{length}}nt.1.ebwt'.format(mature)
	output: 
		sam='data/{sample}_isomir_{length}nt_alignment.sam',
		unmapped='data/{sample}_isomir_{length}nt_unmapped.fastq'
	params:
		prefix = lambda wildcards: get_prefix(wildcards)
	threads: 4
	shell:
		"""bowtie -p {threads} --sam --best --norc -v 3 --chunkmbs 1024 -m 1 -t {params.prefix}\
		{input.file} --un {output.unmapped} > {output.sam}"""

rule unmapped_fastq_merging:
	input: expand('data/{{sample}}_isomir_{length}nt_unmapped.fastq', length=lengths)
	output: 'data/{sample}_isomir_unmapped.fastq' 
	shell:
		"""cat {input} > {output}"""	

rule sam_processing_length_merging:
	input: expand('data/{{sample}}_isomir_{length}nt_alignment.sam',length=lengths)
	output: 'data/{sample}_isomir_reads.txt'
	shell:
		"""awk -f src/sam_processing_length_merging.awk {input} > {output}"""

rule isomir_read_counting:
	input: 'data/{sample}_isomir_reads.txt'
	output: 'data/{sample}_isomir_readcount.txt'
	shell:
		"""awk -f src/isomir_read_counting.awk {input} > {output}"""

rule count_merging:
	input: expand('data/{sample}_isomir_readcount.txt',sample=samples.index)
	output: 'isomir_readcount.txt'
	params: samples = lambda wildcards: samples.index
	script:
		"""src/count_merging.R"""

